package com.sarse.sarse.entities;

import javax.persistence.*;


@Entity
@Table(name = "solicitud")
public class Solicitud {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_solicitud")
    private Integer id_solicitud;

    @ManyToOne
    @JoinColumn(name = "id_solicitante")
    private Usuario solicitante;

    @ManyToOne
    @JoinColumn(name = "id_admin")
    private Usuario admin;

    @ManyToOne
    @JoinColumn(name = "id_nuevo")
    private Usuario nuevo;

    @ManyToOne
    @JoinColumn(name = "id_reclamo")
    private Reclamo reclamo;

    @Column(name = "estado")	
    private Integer estado;

    @Lob
    @Column(name = "motivo",length = 7000)	
    private String motivo;

    @Lob
    @Column(name = "comentario",length = 7000)	
    private String comentario;


    public Solicitud() {
    }

    public Solicitud(Integer id_solicitud, Usuario solicitante, Usuario admin, Usuario nuevo, Reclamo reclamo, Integer estado, String motivo, String comentario) {
        this.id_solicitud = id_solicitud;
        this.solicitante = solicitante;
        this.admin = admin;
        this.nuevo = nuevo;
        this.reclamo = reclamo;
        this.estado = estado;
        this.motivo = motivo;
        this.comentario = comentario;
    }

    public Integer getId_solicitud() {
        return this.id_solicitud;
    }

    public void setId_solicitud(Integer id_solicitud) {
        this.id_solicitud = id_solicitud;
    }

    public Usuario getSolicitante() {
        return this.solicitante;
    }

    public void setSolicitante(Usuario solicitante) {
        this.solicitante = solicitante;
    }

    public Usuario getAdmin() {
        return this.admin;
    }

    public void setAdmin(Usuario admin) {
        this.admin = admin;
    }

    public Usuario getNuevo() {
        return this.nuevo;
    }

    public void setNuevo(Usuario nuevo) {
        this.nuevo = nuevo;
    }

    public Reclamo getReclamo() {
        return this.reclamo;
    }

    public void setReclamo(Reclamo reclamo) {
        this.reclamo = reclamo;
    }

    public Integer getEstado() {
        return this.estado;
    }

    public void setEstado(Integer estado) {
        this.estado = estado;
    }

    public String getMotivo() {
        return this.motivo;
    }

    public void setMotivo(String motivo) {
        this.motivo = motivo;
    }

    public String getComentario() {
        return this.comentario;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }

    
}