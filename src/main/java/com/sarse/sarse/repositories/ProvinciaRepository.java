package com.sarse.sarse.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

import com.sarse.sarse.entities.Provincia;

@Repository
public interface ProvinciaRepository extends JpaRepository<Provincia, Integer> {
    
    @Query(value = "SELECT * FROM provincia a WHERE a.id_departamento = ?1", nativeQuery = true)
    List<Provincia> findbyDepartamento(Integer id_departamento);
    
}