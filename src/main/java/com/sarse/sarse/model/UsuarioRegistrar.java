package com.sarse.sarse.model;
public class UsuarioRegistrar {
    private Integer id;
    private String nombre;
    private String apellido_pat;
    private String apellido_mat;
    private Integer tipo_doc;
    private String num_doc;
    private Integer rol;
    private String correo;
    private String telefono;
    private Integer distrito;
    private String foto;
    private String pass;


    public UsuarioRegistrar() {
    }


    public UsuarioRegistrar(Integer id, String nombre, String apellido_pat, String apellido_mat, Integer tipo_doc, String num_doc, Integer rol, String correo, String telefono, Integer distrito, String foto) {
        this.id = id;
        this.nombre = nombre;
        this.apellido_pat = apellido_pat;
        this.apellido_mat = apellido_mat;
        this.tipo_doc = tipo_doc;
        this.num_doc = num_doc;
        this.rol = rol;
        this.correo = correo;
        this.telefono = telefono;
        this.distrito = distrito;
        this.foto = foto;
    }


    public UsuarioRegistrar(Integer id, String nombre, String apellido_pat, String apellido_mat, Integer tipo_doc, String num_doc, Integer rol, String correo, String telefono, Integer distrito, String foto, String pass) {
        this.id = id;
        this.nombre = nombre;
        this.apellido_pat = apellido_pat;
        this.apellido_mat = apellido_mat;
        this.tipo_doc = tipo_doc;
        this.num_doc = num_doc;
        this.rol = rol;
        this.correo = correo;
        this.telefono = telefono;
        this.distrito = distrito;
        this.foto = foto;
        this.pass = pass;
    }

    public String getPass() {
        return this.pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }


    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombre() {
        return this.nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido_pat() {
        return this.apellido_pat;
    }

    public void setApellido_pat(String apellido_pat) {
        this.apellido_pat = apellido_pat;
    }

    public String getApellido_mat() {
        return this.apellido_mat;
    }

    public void setApellido_mat(String apellido_mat) {
        this.apellido_mat = apellido_mat;
    }

    public Integer getTipo_doc() {
        return this.tipo_doc;
    }

    public void setTipo_doc(Integer tipo_doc) {
        this.tipo_doc = tipo_doc;
    }

    public String getNum_doc() {
        return this.num_doc;
    }

    public void setNum_doc(String num_doc) {
        this.num_doc = num_doc;
    }

    public Integer getRol() {
        return this.rol;
    }

    public void setRol(Integer rol) {
        this.rol = rol;
    }

    public String getCorreo() {
        return this.correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getTelefono() {
        return this.telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public Integer getDistrito() {
        return this.distrito;
    }

    public void setDistrito(Integer distrito) {
        this.distrito = distrito;
    }

    public String getFoto() {
        return this.foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

}