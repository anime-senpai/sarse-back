package com.sarse.sarse.model;
public class Comentario {
    private Integer id;
    private String texto;
    private Integer motivo;


    public Comentario() {
    }

    public Comentario(Integer id, String texto, Integer motivo) {
        this.id = id;
        this.texto = texto;
        this.motivo = motivo;
    }

    public Integer getMotivo() {
        return this.motivo;
    }

    public void setMotivo(Integer motivo) {
        this.motivo = motivo;
    }

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTexto() {
        return this.texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }

}