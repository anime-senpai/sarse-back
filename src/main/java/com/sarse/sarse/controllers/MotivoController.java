package com.sarse.sarse.controller;
import java.io.*; 

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;

import com.sarse.sarse.entities.Motivo;
import com.sarse.sarse.services.MotivoService;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

@RestController
@RequestMapping(value = "/motivo")
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.DELETE, RequestMethod.PUT})
public class MotivoController {

    @Autowired
    private MotivoService motivoService;

    @RequestMapping(value = "", method = RequestMethod.GET)
    public List<Motivo> listMotivos() {
        List<Motivo> lista = motivoService.getMotivos();
        return lista;
    }
    
}