package com.sarse.sarse.services;

import java.util.ArrayList;
import java.util.List;

import com.sarse.sarse.entities.Provincia;
import com.sarse.sarse.repositories.ProvinciaRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProvinciaService {

    @Autowired
    private ProvinciaRepository provinciaRepository;

    public List<Provincia> getProvincias() {
        return provinciaRepository.findAll();
    }

    public List<Provincia> getProvinciasxDepartamento(Integer id_departamento) {
        return provinciaRepository.findbyDepartamento(id_departamento);
    }
    
    
}