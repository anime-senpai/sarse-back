package com.sarse.sarse.entities;

import javax.persistence.*;

@Entity
@Table(name = "evidencia")
public class Evidencia {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_evidencia")
    private Integer id_evidencia;

    @Column(name = "nombre",length = 100)	
    private String nombre;

    @ManyToOne
    @JoinColumn(name = "id_reclamo")
    private Reclamo reclamo;

    @Column(name = "ubicacion",length = 250)	
    private String ubicacion;


    public Evidencia() {
    }

    public Evidencia(Integer id_evidencia, String nombre, Reclamo reclamo, String ubicacion) {
        this.id_evidencia = id_evidencia;
        this.nombre = nombre;
        this.reclamo = reclamo;
        this.ubicacion = ubicacion;
    }

    public Integer getId_evidencia() {
        return this.id_evidencia;
    }

    public void setId_evidencia(Integer id_evidencia) {
        this.id_evidencia = id_evidencia;
    }

    public String getNombre() {
        return this.nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Reclamo getReclamo() {
        return this.reclamo;
    }

    public void setReclamo(Reclamo reclamo) {
        this.reclamo = reclamo;
    }

    public String getUbicacion() {
        return this.ubicacion;
    }

    public void setUbicacion(String ubicacion) {
        this.ubicacion = ubicacion;
    }

}