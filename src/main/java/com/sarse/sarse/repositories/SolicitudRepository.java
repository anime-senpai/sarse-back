package com.sarse.sarse.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

import com.sarse.sarse.entities.Solicitud;

@Repository
public interface SolicitudRepository extends JpaRepository<Solicitud, Integer> {
    
    @Query(value = "SELECT * FROM solicitud a WHERE a.id_solicitante = ?1", nativeQuery = true)
    List<Solicitud> findbyEspecialista(Integer idUsuario);

    @Query(value = "SELECT * FROM solicitud a WHERE a.id_solicitud = ?1", nativeQuery = true)
    Solicitud findbyId1(Integer idSolicitud);
    
}