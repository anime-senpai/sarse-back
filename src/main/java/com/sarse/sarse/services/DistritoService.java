package com.sarse.sarse.services;

import java.util.ArrayList;
import java.util.List;

import com.sarse.sarse.entities.Distrito;
import com.sarse.sarse.repositories.DistritoRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DistritoService {

    @Autowired
    private DistritoRepository distritoRepository;

    public List<Distrito> getDistritos() {
        return distritoRepository.findAll();
    }

    public List<Distrito> getDistritosbyProvincia(Integer id_provincia) {
        return distritoRepository.findbyProvincia(id_provincia);
    }
    
    
}