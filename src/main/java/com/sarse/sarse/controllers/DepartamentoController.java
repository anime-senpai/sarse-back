package com.sarse.sarse.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;

import com.sarse.sarse.entities.Departamento;
import com.sarse.sarse.services.DepartamentoService;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

@RestController
@RequestMapping(value = "/departamento")
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.DELETE, RequestMethod.PUT})
public class DepartamentoController {

    @Autowired
    private DepartamentoService departamentoService;

    @RequestMapping(value = "", method = RequestMethod.GET)
    public List<Departamento> listDepartamentos() {
        List<Departamento> lista = departamentoService.getDepartamentos();
        return lista;
    }
    
}