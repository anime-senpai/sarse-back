package com.sarse.sarse.model;
public class ReclamoRegistrar {
    
    private Integer usuarioRegistrar;
    private Integer suministradorRegistrar;
    private Integer departamentoRegistrar;
    private Integer provinciaRegistrar;
    private Integer distritoRegistrar;
    private String domicilioRegistrar;
    private String reclamoRegistrar;
    private String numSuministroRegistrar;

    public ReclamoRegistrar() {
    }


    public ReclamoRegistrar(Integer usuarioRegistrar, Integer suministradorRegistrar, Integer departamentoRegistrar, Integer provinciaRegistrar, Integer distritoRegistrar, String domicilioRegistrar, String reclamoRegistrar, String numSuministroRegistrar) {
        this.usuarioRegistrar = usuarioRegistrar;
        this.suministradorRegistrar = suministradorRegistrar;
        this.departamentoRegistrar = departamentoRegistrar;
        this.provinciaRegistrar = provinciaRegistrar;
        this.distritoRegistrar = distritoRegistrar;
        this.domicilioRegistrar = domicilioRegistrar;
        this.reclamoRegistrar = reclamoRegistrar;
        this.numSuministroRegistrar = numSuministroRegistrar;
    }


    public String getNumSuministroRegistrar() {
        return this.numSuministroRegistrar;
    }

    public void setNumSuministroRegistrar(String numSuministroRegistrar) {
        this.numSuministroRegistrar = numSuministroRegistrar;
    }

    public Integer getUsuarioRegistrar() {
        return this.usuarioRegistrar;
    }

    public void setUsuarioRegistrar(Integer usuarioRegistrar) {
        this.usuarioRegistrar = usuarioRegistrar;
    }

    public Integer getSuministradorRegistrar() {
        return this.suministradorRegistrar;
    }

    public void setSuministradorRegistrar(Integer suministradorRegistrar) {
        this.suministradorRegistrar = suministradorRegistrar;
    }

    public Integer getDepartamentoRegistrar() {
        return this.departamentoRegistrar;
    }

    public void setDepartamentoRegistrar(Integer departamentoRegistrar) {
        this.departamentoRegistrar = departamentoRegistrar;
    }

    public Integer getProvinciaRegistrar() {
        return this.provinciaRegistrar;
    }

    public void setProvinciaRegistrar(Integer provinciaRegistrar) {
        this.provinciaRegistrar = provinciaRegistrar;
    }

    public Integer getDistritoRegistrar() {
        return this.distritoRegistrar;
    }

    public void setDistritoRegistrar(Integer distritoRegistrar) {
        this.distritoRegistrar = distritoRegistrar;
    }

    public String getDomicilioRegistrar() {
        return this.domicilioRegistrar;
    }

    public void setDomicilioRegistrar(String domicilioRegistrar) {
        this.domicilioRegistrar = domicilioRegistrar;
    }

    public String getReclamoRegistrar() {
        return this.reclamoRegistrar;
    }

    public void setReclamoRegistrar(String reclamoRegistrar) {
        this.reclamoRegistrar = reclamoRegistrar;
    }
    
}