package com.sarse.sarse.controller;
import java.io.*; 

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;

import com.sarse.sarse.entities.Solicitud;
import com.sarse.sarse.services.SolicitudService;
import com.sarse.sarse.model.SolicitudRegistrar;
import com.sarse.sarse.model.RespuestaReasignacion;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

@RestController
@RequestMapping(value = "/solicitud")
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.DELETE, RequestMethod.PUT})
public class SolicitudController {

    @Autowired
    private SolicitudService solicitudService;

    @RequestMapping(value = "", method = RequestMethod.GET)
    public List<Solicitud> listSolicitudes() {
        List<Solicitud> lista = solicitudService.getSolicitudes();
        return lista;
    }

    @RequestMapping(value = "/especialista/{idUsuario}", method = RequestMethod.GET)
    public List<Solicitud> listSolicitudesbyEspecialista(@PathVariable(value = "idUsuario") Integer idUsuario) {
        List<Solicitud> lista = solicitudService.getSolicitudesxEspecialista(idUsuario);
        return lista;
    }

    @RequestMapping(value = "/registrar", method = RequestMethod.POST)
    public Integer registrarSolicitud(@RequestBody SolicitudRegistrar solicitud) {
        Integer id_solicitud = solicitudService.registrarSolicitud(solicitud);
        return id_solicitud;
    }

    @RequestMapping(value = "/cancelar/{idSolicitud}", method = RequestMethod.POST)
    public Integer cancelarSolicitud(@PathVariable(value = "idSolicitud") Integer idSolicitud) {
        return solicitudService.cancelarSolicitud(idSolicitud);
    }

    @RequestMapping(value = "/aceptar", method = RequestMethod.POST)
    public Integer aceptarSolicitud(@RequestBody RespuestaReasignacion solicitud) {
        Integer id_solicitud = solicitudService.aceptarSolicitud(solicitud);
        return id_solicitud;
    }

    @RequestMapping(value = "/denegar", method = RequestMethod.POST)
    public Integer denegarSolicitud(@RequestBody RespuestaReasignacion solicitud) {
        Integer id_solicitud = solicitudService.denegarSolicitud(solicitud);
        return id_solicitud;
    }
    
}