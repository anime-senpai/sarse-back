package com.sarse.sarse.model;
public class CambioPass {
    private Integer idUsuario;
    private String passAntiguo;
    private String passNuevo;


    public CambioPass() {
    }

    public CambioPass(Integer idUsuario, String passAntiguo, String passNuevo) {
        this.idUsuario = idUsuario;
        this.passAntiguo = passAntiguo;
        this.passNuevo = passNuevo;
    }

    public Integer getIdUsuario() {
        return this.idUsuario;
    }

    public void setIdUsuario(Integer idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getPassAntiguo() {
        return this.passAntiguo;
    }

    public void setPassAntiguo(String passAntiguo) {
        this.passAntiguo = passAntiguo;
    }

    public String getPassNuevo() {
        return this.passNuevo;
    }

    public void setPassNuevo(String passNuevo) {
        this.passNuevo = passNuevo;
    }

}