package com.sarse.sarse.entities;

import javax.persistence.*;

@Entity
@Table(name = "motivo")
public class Motivo {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_motivo")
    private Integer id_motivo;

    @Column(name = "desc",length = 100)	
    private String desc;


    public Motivo() {
    }


    public Motivo(Integer id_motivo, String desc) {
        this.id_motivo = id_motivo;
        this.desc = desc;
    }
    
    public Integer getId_motivo() {
        return this.id_motivo;
    }

    public void setId_motivo(Integer id_motivo) {
        this.id_motivo = id_motivo;
    }

    public String getDesc() {
        return this.desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    
}