package com.sarse.sarse.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;

import com.sarse.sarse.entities.Distrito;
import com.sarse.sarse.services.DistritoService;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

@RestController
@RequestMapping(value = "/distrito")
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.DELETE, RequestMethod.PUT})
public class DistritoController {

    @Autowired
    private DistritoService distritoService;

    @RequestMapping(value = "", method = RequestMethod.GET)
    public List<Distrito> listDistritos() {
        List<Distrito> lista = distritoService.getDistritos();
        return lista;
    }

    @RequestMapping(value = "/provincia/{id_provincia}", method = RequestMethod.GET)
    public List<Distrito> listDistritosxProvincia(@PathVariable(value = "id_provincia") Integer id_provincia) {
        List<Distrito> lista = distritoService.getDistritosbyProvincia(id_provincia);
        return lista;
    }
    
}