package com.sarse.sarse.model;
public class EditarCuenta {
    private Integer idUsuario;
    private String telefono;
    private String foto;


    public EditarCuenta() {
    }

    public EditarCuenta(Integer idUsuario, String telefono, String foto) {
        this.idUsuario = idUsuario;
        this.telefono = telefono;
        this.foto = foto;
    }


    public Integer getIdUsuario() {
        return this.idUsuario;
    }

    public void setIdUsuario(Integer idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getTelefono() {
        return this.telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getFoto() {
        return this.foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }
    
}