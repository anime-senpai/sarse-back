package com.sarse.sarse.services;

import java.util.ArrayList;
import java.util.List;

import com.sarse.sarse.entities.Suministrador;
import com.sarse.sarse.repositories.SuministradorRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SuministradorService {

    @Autowired
    private SuministradorRepository suministradorRepository;

    public List<Suministrador> getSuministradores() {
        return suministradorRepository.findAll();
    }
    
}