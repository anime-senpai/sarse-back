package com.sarse.sarse.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;

import com.sarse.sarse.entities.Suministrador;
import com.sarse.sarse.services.SuministradorService;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

@RestController
@RequestMapping(value = "/suministrador")
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.DELETE, RequestMethod.PUT})
public class SuministradorController {

    @Autowired
    private SuministradorService suministradorService;

    @RequestMapping(value = "", method = RequestMethod.GET)
    public List<Suministrador> listSuministradores() {
        List<Suministrador> lista = suministradorService.getSuministradores();
        return lista;
    }
    
}