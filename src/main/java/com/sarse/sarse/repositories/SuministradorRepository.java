package com.sarse.sarse.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

import com.sarse.sarse.entities.Suministrador;

@Repository
public interface SuministradorRepository extends JpaRepository<Suministrador, Integer> {
    
}