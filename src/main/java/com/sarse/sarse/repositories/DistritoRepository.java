package com.sarse.sarse.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

import com.sarse.sarse.entities.Distrito;

@Repository
public interface DistritoRepository extends JpaRepository<Distrito, Integer> {
    
    @Query(value = "SELECT * FROM distrito a WHERE a.id_provincia = ?1", nativeQuery = true)
    List<Distrito> findbyProvincia(Integer id_provincia);
    
}