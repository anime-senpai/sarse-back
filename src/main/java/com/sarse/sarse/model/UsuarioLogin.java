package com.sarse.sarse.model;
public class UsuarioLogin {
    private String correo;
    private String password;


    public UsuarioLogin() {
    }

    public UsuarioLogin(String correo, String password) {
        this.correo = correo;
        this.password = password;
    }

    public String getCorreo() {
        return this.correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}