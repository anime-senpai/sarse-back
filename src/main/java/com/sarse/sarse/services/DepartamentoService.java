package com.sarse.sarse.services;

import java.util.ArrayList;
import java.util.List;

import com.sarse.sarse.entities.Departamento;
import com.sarse.sarse.repositories.DepartamentoRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DepartamentoService {

    @Autowired
    private DepartamentoRepository departamentoRepository;

    public List<Departamento> getDepartamentos() {
        return departamentoRepository.findAll();
    }
    
    
}