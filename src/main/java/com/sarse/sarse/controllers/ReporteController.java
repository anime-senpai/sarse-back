package com.sarse.sarse.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;

import com.sarse.sarse.model.Reporte;
import com.sarse.sarse.services.ReporteService;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

@RestController
@RequestMapping(value = "/reporte")
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.DELETE, RequestMethod.PUT})
public class ReporteController {

    @Autowired
    private ReporteService reporteService;

    @RequestMapping(value = "/motivo", method = RequestMethod.GET)
    public List<Reporte> reporteMotivo() {
        List<Reporte> lista = reporteService.reporteMotivo();
        return lista;
    }

    @RequestMapping(value = "/dia", method = RequestMethod.GET)
    public List<Reporte> reporteDia() {
        List<Reporte> lista = reporteService.reporteDia();
        return lista;
    }

    @RequestMapping(value = "/tiempo", method = RequestMethod.GET)
    public List<Reporte> reporteTiempo() {
        List<Reporte> lista = reporteService.reporteTiempo();
        return lista;
    }
    
}