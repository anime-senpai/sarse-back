package com.sarse.sarse.entities;

import javax.persistence.*;

import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

@Entity
@Table(name = "reclamo")
public class Reclamo {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_reclamo")
    private Integer id_reclamo;

    @ManyToOne
    @JoinColumn(name = "id_motivo")
    private Motivo motivo;

    @ManyToOne
    @JoinColumn(name = "id_especialista")
    private Usuario especialista;

    @ManyToOne
    @JoinColumn(name = "id_ciudadano")
    private Usuario ciudadano;

    @Column(name = "fecha_registro")	
    private Date fecha_registro;

    @Column(name = "fecha_respuesta")	
    private Date fecha_respuesta;

    @Lob
    @Column(name = "texto",length = 7000)	
    private String texto;

    @Lob
    @Column(name = "respuesta",length = 7000)	
    private String respuesta;

    @ManyToOne
    @JoinColumn(name = "id_suministrador")
    private Suministrador suministrador;

    @Column(name = "num_suministro",length = 20)	
    private String num_suministro;

    @ManyToOne
    @JoinColumn(name = "id_distrito")
    private Distrito distrito;

    @Column(name = "domicilio",length = 200)	
    private String domicilio;

    @Lob
    @Column(name = "comentario",length = 7000)	
    private String comentario;

    @Column(name = "calificacion_1")	
    private Double calificacion_1;

    @Column(name = "calificacion_2")	
    private Double calificacion_2;

    @Column(name = "calificacion_3")	
    private Double calificacion_3;

    @Column(name = "estado")	
    private Integer estado;

    @Transient
    private String fecha_registro_str;

    @Transient
    private String fecha_respuesta_str;



    public void setFecha_registro_str() {
        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        this.fecha_registro_str = dateFormat.format(this.fecha_registro);
    }

    public void setFecha_respuesta_str() {
        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        this.fecha_respuesta_str = dateFormat.format(this.fecha_respuesta);
    }


    public Reclamo() {
        if(this.fecha_registro != null)
            this.setFecha_registro_str();
        if(this.fecha_respuesta != null)
            this.setFecha_respuesta_str();
    }


    public String getNum_suministro() {
        return this.num_suministro;
    }

    public void setNum_suministro(String num_suministro) {
        this.num_suministro = num_suministro;
    }

    public Reclamo(Integer id_reclamo, Motivo motivo, Usuario especialista, Usuario ciudadano, Date fecha_registro, Date fecha_respuesta, String texto, String respuesta, Suministrador suministrador, Distrito distrito, String domicilio, String comentario, Double calificacion_1, Double calificacion_2, Double calificacion_3, Integer estado, String num_suministro) {
        this.id_reclamo = id_reclamo;
        this.motivo = motivo;
        this.especialista = especialista;
        this.ciudadano = ciudadano;
        this.fecha_registro = fecha_registro;
        this.fecha_respuesta = fecha_respuesta;
        this.texto = texto;
        this.respuesta = respuesta;
        this.suministrador = suministrador;
        this.distrito = distrito;
        this.domicilio = domicilio;
        this.comentario = comentario;
        this.calificacion_1 = calificacion_1;
        this.calificacion_2 = calificacion_2;
        this.calificacion_3 = calificacion_3;
        this.estado = estado;
        this.num_suministro = num_suministro;
        if(this.fecha_registro != null)
            this.setFecha_registro_str();
        if(this.fecha_respuesta != null)
            this.setFecha_respuesta_str();
    }



    public String getFecha_registro_str() {
        return this.fecha_registro_str;
    }

    public void setFecha_registro_str(String fecha_registro_str) {
        this.fecha_registro_str = fecha_registro_str;
    }

    public String getFecha_respuesta_str() {
        return this.fecha_respuesta_str;
    }

    public void setFecha_respuesta_str(String fecha_respuesta_str) {
        this.fecha_respuesta_str = fecha_respuesta_str;
    }


    public Integer getId_reclamo() {
        return this.id_reclamo;
    }

    public void setId_reclamo(Integer id_reclamo) {
        this.id_reclamo = id_reclamo;
    }

    public Motivo getMotivo() {
        return this.motivo;
    }

    public void setMotivo(Motivo motivo) {
        this.motivo = motivo;
    }

    public Usuario getEspecialista() {
        return this.especialista;
    }

    public void setEspecialista(Usuario especialista) {
        this.especialista = especialista;
    }

    public Usuario getCiudadano() {
        return this.ciudadano;
    }

    public void setCiudadano(Usuario ciudadano) {
        this.ciudadano = ciudadano;
    }

    public Date getFecha_registro() {
        return this.fecha_registro;
    }

    public void setFecha_registro(Date fecha_registro) {
        this.fecha_registro = fecha_registro;
    }

    public Date getFecha_respuesta() {
        return this.fecha_respuesta;
    }

    public void setFecha_respuesta(Date fecha_respuesta) {
        this.fecha_respuesta = fecha_respuesta;
    }

    public String getTexto() {
        return this.texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }

    public String getRespuesta() {
        return this.respuesta;
    }

    public void setRespuesta(String respuesta) {
        this.respuesta = respuesta;
    }

    public Suministrador getSuministrador() {
        return this.suministrador;
    }

    public void setSuministrador(Suministrador suministrador) {
        this.suministrador = suministrador;
    }

    public Distrito getDistrito() {
        return this.distrito;
    }

    public void setDistrito(Distrito distrito) {
        this.distrito = distrito;
    }

    public String getDomicilio() {
        return this.domicilio;
    }

    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }

    public String getComentario() {
        return this.comentario;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }

    public Double getCalificacion_1() {
        return this.calificacion_1;
    }

    public void setCalificacion_1(Double calificacion_1) {
        this.calificacion_1 = calificacion_1;
    }

    public Double getCalificacion_2() {
        return this.calificacion_2;
    }

    public void setCalificacion_2(Double calificacion_2) {
        this.calificacion_2 = calificacion_2;
    }

    public Double getCalificacion_3() {
        return this.calificacion_3;
    }

    public void setCalificacion_3(Double calificacion_3) {
        this.calificacion_3 = calificacion_3;
    }

    public Integer getEstado() {
        return this.estado;
    }

    public void setEstado(Integer estado) {
        this.estado = estado;
    }

}