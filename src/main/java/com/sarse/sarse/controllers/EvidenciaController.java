package com.sarse.sarse.controller;
import java.io.*; 

import org.springframework.http.MediaType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import java.util.List;
import java.util.ArrayList;

import com.sarse.sarse.entities.Evidencia;
import com.sarse.sarse.entities.Reclamo;
import com.sarse.sarse.services.EvidenciaService;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

@RestController
@RequestMapping(value = "/evidencia")
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.DELETE, RequestMethod.PUT})
public class EvidenciaController {

    @Autowired
    private EvidenciaService evidenciaService;

    //private static String UPLOADED_FOLDER = "C:\Users\andre\OneDrive\Desktop\PUCP\Tesis\Sarse\evidencias";
    private static String UPLOADED_FOLDER = "C:/Users/andre/OneDrive/Desktop/PUCP/Tesis/Sarse/front/ejemplo-router/vue-material-admin/public/static/evidencias";

    @RequestMapping(value = "", method = RequestMethod.GET)
    public List<Evidencia> listEvidencias() {
        List<Evidencia> lista = evidenciaService.getEvidencias();
        return lista;
    }

    @RequestMapping(value = "/reclamo/{id_reclamo}", method = RequestMethod.GET)
    public List<Evidencia> listEvidenciasxReclamo(@PathVariable(value = "id_reclamo") Integer id_reclamo) {
        List<Evidencia> lista = evidenciaService.getEvidenciasbyReclamo(id_reclamo);
        return lista;
    }

    @RequestMapping(value = "/registrar/{id_reclamo}", method = RequestMethod.POST)
    public Integer registrarEvidencia(@RequestParam("file") MultipartFile file, @PathVariable("id_reclamo") Integer id_reclamo) {
        File dir = new File(UPLOADED_FOLDER);
        Reclamo r = new Reclamo();
        String nombre = "reclamo_"+ id_reclamo.toString()+file.getOriginalFilename();
        r.setId_reclamo(id_reclamo);

        Evidencia ev = new Evidencia();
        try {
            byte[] bytes = file.getBytes();

            if (!dir.exists())
                dir.mkdirs();

            File uploadFile = new File(dir.getAbsolutePath()
                    + File.separator + nombre);
            BufferedOutputStream outputStream = new BufferedOutputStream(
                    new FileOutputStream(uploadFile));
            outputStream.write(bytes);
            outputStream.close();
            
            ev.setNombre(file.getOriginalFilename());
            ev.setReclamo(r);
            ev.setUbicacion(nombre);

        } catch (Exception e) {
            System.out.println("fracaso");
            System.out.println(e);
            return -1;
        }
        
        
		return evidenciaService.guardarEvidencia(ev);
	}
    
}