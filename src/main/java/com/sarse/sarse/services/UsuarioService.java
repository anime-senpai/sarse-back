package com.sarse.sarse.services;

import java.util.*; 

import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import java.nio.charset.*;

import com.sarse.sarse.entities.*;
import com.sarse.sarse.model.UsuarioLogin;
import com.sarse.sarse.model.UsuarioDNI;
import com.sarse.sarse.model.UsuarioRegistrar;
import com.sarse.sarse.model.CambioPass;
import com.sarse.sarse.model.EditarCuenta;
import com.sarse.sarse.repositories.UsuarioRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.net.HttpURLConnection;
import java.net.URL;
import com.google.gson.Gson;

import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;


@Service
public class UsuarioService {
    
    @Autowired
    private UsuarioRepository usuarioRepository;
	
	private EncryptService encriptador;

	@Autowired
    private JavaMailSender javaMailSender;




    private static String streamToString(InputStream inputStream) {
        String text = new Scanner(inputStream, "UTF-8").useDelimiter("\\Z").next();
        return text;
    }


    public void enviarCorreo(String correo, String asunto, String mensaje){
		
        SimpleMailMessage msg = new SimpleMailMessage();
        msg.setTo(correo);
        msg.setFrom("sarsepucp@gmail.com");
        msg.setSubject(asunto);
        msg.setText(mensaje);

        javaMailSender.send(msg);
        
    }

    public Integer registrarUsuario(UsuarioRegistrar u) {
        Rol r = new Rol();
        TipoDocumento t = new TipoDocumento();
        Distrito d = new Distrito();
        String p = getAlphaNumericString(6);
        String password = EncryptService.encrypt(p,u.getNum_doc());
        Date hoy = new Date();

        r.setId_rol(u.getRol());
        t.setId_tipo_documento(u.getTipo_doc());
        d.setId_distrito(u.getDistrito());

        Usuario usuario = new Usuario();
        usuario.setRol(r);
        usuario.setTipo_documento(t);
        usuario.setNum_documento(u.getNum_doc());
        usuario.setNombre(u.getNombre());
        usuario.setApellido_pat(u.getApellido_pat());
        usuario.setApellido_mat(u.getApellido_mat());
        usuario.setCorreo(u.getCorreo());
        usuario.setTelefono(u.getTelefono());
        usuario.setDistrito(d);
        usuario.setPassword(password);
        usuario.setEstado(1);
        usuario.setFecha_registro(hoy);

        usuario = usuarioRepository.save(usuario);

        String foto = "usuario_"+ usuario.getId_usuario().toString()+".jpg";
        usuario.setFoto(foto);
        usuario = usuarioRepository.save(usuario);

        String asunto = "Bienvenido a Sarse";
        String mensaje = "Bienvenido a sarse su clave de acceso es: \n " + p + "\n Por favor cambiarla lo antes posible\n Atentamente Sarse.";
        enviarCorreo(u.getCorreo(), asunto, mensaje);

        if(u.getRol()==1){
            usuarioRepository.registrarEspecialista(usuario.getId_usuario());
        }

        return usuario.getId_usuario();
    }

    public Integer editarUsuario(UsuarioRegistrar u) {
        Rol r = new Rol();
        TipoDocumento t = new TipoDocumento();
        Distrito d = new Distrito();

        r.setId_rol(u.getRol());
        t.setId_tipo_documento(u.getTipo_doc());
        d.setId_distrito(u.getDistrito());

        Usuario usuario = usuarioRepository.buscarUsuario(u.getId());
        usuario.setRol(r);
        usuario.setTipo_documento(t);
        usuario.setNum_documento(u.getNum_doc());
        usuario.setNombre(u.getNombre());
        usuario.setApellido_pat(u.getApellido_pat());
        usuario.setApellido_mat(u.getApellido_mat());
        usuario.setCorreo(u.getCorreo());
        usuario.setTelefono(u.getTelefono());
        usuario.setDistrito(d);

        usuario = usuarioRepository.save(usuario);
        return usuario.getId_usuario();
    }

    public Integer eliminarUsuario(Integer id_usuario) {
        Usuario usuario = usuarioRepository.buscarUsuario(id_usuario);
        usuario.setEstado(0);
        usuario = usuarioRepository.save(usuario);
        usuarioRepository.eliminarEspecialista(id_usuario);
        return usuario.getId_usuario();
    }

    public Usuario login(UsuarioLogin u) {
        Usuario usuario = usuarioRepository.findByCorreo(u.getCorreo());
        Usuario a = new Usuario();
        a.setId_usuario(0);
        if(usuario == null) return a;
        try{
            String password = EncryptService.encrypt(u.getPassword(),usuario.getNum_documento());
            String pass = usuario.getPassword();
            String pass_temp = usuario.getPassword_temp();
            if(pass.equals(password)) {
                return usuario;
            }else if((pass_temp != null) && pass_temp.equals(password)){
                return usuario;
            }

            return a;
        }catch(Exception e){
            return a;
        }
    }

    public List<Usuario> getUsuarios() {
        return usuarioRepository.findAllActivo();
    }

    public List<Usuario> getUsuariosxRol(Integer id_rol) {
        return usuarioRepository.findAllByRol(id_rol);
    }

    public List<Usuario> getEspAndSup() {
        return usuarioRepository.findAllEspAndSup();
    }

    public Usuario getUsuario(Integer id_usuario) {
        return usuarioRepository.buscarUsuario(id_usuario);
    }

    public Integer registrarCiudadano(UsuarioRegistrar u) {
        Rol r = new Rol();
        TipoDocumento t = new TipoDocumento();
        Date hoy = new Date();
        Distrito d = new Distrito();
        String password = EncryptService.encrypt(u.getPass(),u.getNum_doc());
        d.setId_distrito(150114);

        r.setId_rol(3);
        t.setId_tipo_documento(u.getTipo_doc());

        Usuario usuario = new Usuario();
        usuario.setRol(r);
        usuario.setTipo_documento(t);
        usuario.setNum_documento(u.getNum_doc());
        usuario.setNombre(u.getNombre());
        usuario.setApellido_pat(u.getApellido_pat());
        usuario.setApellido_mat(u.getApellido_mat());
        usuario.setCorreo(u.getCorreo());
        usuario.setTelefono(u.getTelefono());
        usuario.setPassword(password);
        usuario.setEstado(1);
        usuario.setDistrito(d);
        usuario.setFecha_registro(hoy);

        usuario = usuarioRepository.save(usuario);
        String foto;
        if(u.getFoto()!=null){
            foto = "usuario_"+ usuario.getId_usuario().toString()+".jpg";
        }else{
            foto = "default.png";
        }
        usuario.setFoto(foto);
        usuario = usuarioRepository.save(usuario);

        return usuario.getId_usuario();
    }

    public Integer validarCorreo(String correo) {
        Usuario usuario =  usuarioRepository.findByCorreo(correo);
        if(usuario == null) return 1;
        return 0;
    }
    //SIN PROBAR
    public Integer cambiarPass(CambioPass p){
        Usuario usuario = usuarioRepository.buscarUsuario(p.getIdUsuario());
        try{
            String password = EncryptService.encrypt(p.getPassAntiguo(),usuario.getNum_documento());
            String pass = usuario.getPassword();
            String pass_temp = usuario.getPassword_temp();
            if(pass.equals(password)) {
                String nuevoPass = EncryptService.encrypt(p.getPassNuevo(),usuario.getNum_documento());
                usuario.setPassword(nuevoPass);
                usuario.setPassword_temp(null);
                usuario = usuarioRepository.save(usuario);
                return 1;
                
            }else if((pass_temp != null) && pass_temp.equals(password)){
                String nuevoPass = EncryptService.encrypt(p.getPassNuevo(),usuario.getNum_documento());
                usuario.setPassword(nuevoPass);
                usuario.setPassword_temp(null);
                usuario = usuarioRepository.save(usuario);
                return 1;

            }else{
                return 0;
            }
    
        }catch(Exception e){
            return 0;
        }

    }

    public Integer editarCuenta(EditarCuenta u){
        Usuario usuario = usuarioRepository.buscarUsuario(u.getIdUsuario());
        usuario.setTelefono(u.getTelefono());
        if(u.getFoto() != null){
            usuario.setFoto("usuario_"+u.getIdUsuario().toString()+".jpg");
        }
        usuario = usuarioRepository.save(usuario);
        return u.getIdUsuario();
    }

    public UsuarioDNI validarDNI(String dni, Integer cui){
        String jsonString = null;
        String cadena = "https://api.reniec.cloud/dni/" + dni;
        try {
            URL url = new URL(cadena);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoOutput(true);
            connection.setInstanceFollowRedirects(false);
            //connection.setConnectTimeout(500000);
            connection.setRequestMethod("GET");
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setRequestProperty("charset", "utf-8");
            connection.connect();
            InputStream inStream = connection.getInputStream();
            jsonString = streamToString(inStream); // input stream to string
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }

        Gson g = new Gson();
        UsuarioDNI usuarioDNI = g.fromJson(jsonString, UsuarioDNI.class);
        if(usuarioDNI.getCui()==cui){
            return usuarioDNI;
        }else{
            return null;
        }


    }

    public Integer olvidarPass(String correo){
        Usuario usuario = usuarioRepository.findByCorreo(correo);
        try{
            String p = getAlphaNumericString(6);
            String password = EncryptService.encrypt(p,usuario.getNum_documento());
            usuario.setPassword_temp(password);
            usuario = usuarioRepository.save(usuario);
            String asunto = "Olvidó contraseña Sarse";
            String mensaje = "Por petición suya se le ha generado la siguiente contraseña temporal: \n " + p + "\n Por favor cambiarla lo antes posible\n Atentamente Sarse.";
            enviarCorreo(correo, asunto, mensaje);
            return 1;
        }catch(Exception e){
            return 0;
        }


    }
	
    private static String getAlphaNumericString(int n){   
        byte[] array = new byte[256]; 
        new Random().nextBytes(array); 
  
        String randomString= new String(array, Charset.forName("UTF-8")); 
        StringBuffer r = new StringBuffer(); 
        for (int k = 0; k < randomString.length(); k++) { 
  
            char ch = randomString.charAt(k); 
  
            if (((ch >= 'a' && ch <= 'z') 
                 || (ch >= 'A' && ch <= 'Z') 
                 || (ch >= '0' && ch <= '9')) 
                && (n > 0)) { 
  
                r.append(ch); 
                n--; 
            } 
        } 
        return r.toString(); 
    }
}