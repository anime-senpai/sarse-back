package com.sarse.sarse.services;

import java.util.ArrayList;
import java.util.List;

import com.sarse.sarse.entities.Evidencia;
import com.sarse.sarse.repositories.EvidenciaRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EvidenciaService {

    @Autowired
    private EvidenciaRepository evidenciaRepository;

    public List<Evidencia> getEvidencias() {
        return evidenciaRepository.findAll();
    }

    public List<Evidencia> getEvidenciasbyReclamo(Integer id_reclamo) {
        return evidenciaRepository.findbyReclamo(id_reclamo);
    }

    public Integer guardarEvidencias(List<Evidencia> lista) {
        Evidencia e = new Evidencia();

        for (int i=0; i< lista.size(); i++) {
            e = lista.get(i);
            e = evidenciaRepository.save(e);
        }
        
        return 1;
    }

    public Integer guardarEvidencia(Evidencia ev) {
        ev = evidenciaRepository.save(ev);
        return ev.getId_evidencia();
    }
    
}