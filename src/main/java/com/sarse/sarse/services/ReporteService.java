package com.sarse.sarse.services;

import java.util.ArrayList;
import java.util.List;

import com.sarse.sarse.model.Reporte;
import com.sarse.sarse.repositories.ReporteRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ReporteService {

    @Autowired
    private ReporteRepository reporteRepository;

    public List<Reporte> reporteMotivo() {
        List<Reporte> lista = reporteRepository.reporteMotivo();
        List<Reporte> lista2 = new ArrayList<Reporte>();
        Reporte r = new Reporte();
        Integer suma = 0;
        Integer porc;
        for (int i=0; i< lista.size(); i++) {
            r = lista.get(i);
            suma += r.getCant();
        }
        for (int i=0; i< lista.size(); i++) {
            r = lista.get(i);
            porc = r.getCant()*100/suma;
            r.setPorc(porc);
            lista2.add(r);
        }
        return lista2;
    }

    public List<Reporte> reporteDia() {
        List<Reporte> lista = reporteRepository.reporteDia();
        List<Reporte> lista2 = new ArrayList<Reporte>();
        Reporte r = new Reporte();
        Integer suma =0;
        Integer porc;
        for (int i=0; i< lista.size(); i++) {
            r = lista.get(i);
            suma += r.getCant();
        }
        for (int i=0; i< lista.size(); i++) {
            r = lista.get(i);
            r.setId(i+1);
            porc = r.getCant()*100/suma;
            r.setPorc(porc);
            lista2.add(r);
        }
        return lista2;
    }

    public List<Reporte> reporteTiempo() {
        List<Reporte> lista = reporteRepository.reporteTiempo();
        List<Reporte> lista2 = new ArrayList<Reporte>();
        Reporte r = new Reporte();
        String nombre = new String();
        Integer suma =0;
        Integer porc;
        for (int i=0; i< lista.size(); i++) {
            r = lista.get(i);
            suma += r.getCant();
        }
        for (int i=0; i< lista.size(); i++) {
            r = lista.get(i);
            r.setId(i+1);

            nombre = r.getLabel();
            nombre = nombre.substring(0,nombre.length() - 2);
            if(nombre.equals("1")){
                nombre = nombre + " dia";
            }else{
                nombre = nombre + " dias";

            }
            r.setLabel(nombre);

            porc = r.getCant()*100/suma;
            r.setPorc(porc);

            lista2.add(r);
        }
        return lista2;
    }
    
}