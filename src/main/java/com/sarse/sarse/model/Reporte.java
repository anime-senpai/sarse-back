package com.sarse.sarse.model;

import javax.persistence.*;


@Entity
@Table(name = "reporte")
public class Reporte {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "label",length = 20)
    private String label;
    
    @Column(name = "cant")
    private Integer cant;
    
    @Column(name = "porc")
    private Integer porc;


    public Reporte() {
    }


    public Reporte(Integer id, String label, Integer cant, Integer porc) {
        this.id = id;
        this.label = label;
        this.cant = cant;
        this.porc = porc;
    }

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLabel() {
        return this.label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public Integer getCant() {
        return this.cant;
    }

    public void setCant(Integer cant) {
        this.cant = cant;
    }

    public Integer getPorc() {
        return this.porc;
    }

    public void setPorc(Integer porc) {
        this.porc = porc;
    }

}