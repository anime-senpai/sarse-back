package com.sarse.sarse.entities;

import java.util.Date;
import javax.persistence.*;


@Entity
@Table(name = "usuario")
public class Usuario {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_usuario")
    private Integer id_usuario;

    @ManyToOne
    @JoinColumn(name = "id_rol")
    private Rol rol;

    @ManyToOne
    @JoinColumn(name = "id_tipo_documento")
    private TipoDocumento tipo_documento;

    @Column(name = "num_documento",length = 20)	
    private String num_documento;

    @Column(name = "nombre",length = 100)	
    private String nombre;

    @Column(name = "apellido_pat",length = 100)	
    private String apellido_pat;

    @Column(name = "apellido_mat",length = 100)	
    private String apellido_mat;

    @Column(name = "correo",length = 100)	
    private String correo;

    @Column(name = "telefono",length = 100)	
    private String telefono;

    @ManyToOne
    @JoinColumn(name = "id_distrito")
    private Distrito distrito;

    @Column(name = "password",length = 100)	
    private String password;

    @Column(name = "password_temp",length = 100)	
    private String password_temp;

    @Column(name = "foto")	
    private String foto;

    @Column(name = "estado")	
    private Integer estado;

    @Column(name = "fecha_registro")	
    private Date fecha_registro;
    

    public Usuario() {
    }


    public Usuario(Integer id_usuario, Rol rol, TipoDocumento tipo_documento, String num_documento, String nombre, String apellido_pat, String apellido_mat, String correo, String telefono, Distrito distrito, String password, String password_temp, String foto, Integer estado, Date fecha_registro) {
        this.id_usuario = id_usuario;
        this.rol = rol;
        this.tipo_documento = tipo_documento;
        this.num_documento = num_documento;
        this.nombre = nombre;
        this.apellido_pat = apellido_pat;
        this.apellido_mat = apellido_mat;
        this.correo = correo;
        this.telefono = telefono;
        this.distrito = distrito;
        this.password = password;
        this.password_temp = password_temp;
        this.foto = foto;
        this.estado = estado;
        this.fecha_registro = fecha_registro;
    }

    public String getPassword_temp() {
        return this.password_temp;
    }

    public void setPassword_temp(String password_temp) {
        this.password_temp = password_temp;
    }



    public Date getFecha_registro() {
        return this.fecha_registro;
    }

    public void setFecha_registro(Date fecha_registro) {
        this.fecha_registro = fecha_registro;
    }

    public Integer getEstado() {
        return this.estado;
    }

    public void setEstado(Integer estado) {
        this.estado = estado;
    }


    public Integer getId_usuario() {
        return this.id_usuario;
    }

    public void setId_usuario(Integer id_usuario) {
        this.id_usuario = id_usuario;
    }

    public Rol getRol() {
        return this.rol;
    }

    public void setRol(Rol rol) {
        this.rol = rol;
    }

    public TipoDocumento getTipo_documento() {
        return this.tipo_documento;
    }

    public void setTipo_documento(TipoDocumento tipo_documento) {
        this.tipo_documento = tipo_documento;
    }

    public String getNum_documento() {
        return this.num_documento;
    }

    public void setNum_documento(String num_documento) {
        this.num_documento = num_documento;
    }

    public String getNombre() {
        return this.nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido_pat() {
        return this.apellido_pat;
    }

    public void setApellido_pat(String apellido_pat) {
        this.apellido_pat = apellido_pat;
    }

    public String getApellido_mat() {
        return this.apellido_mat;
    }

    public void setApellido_mat(String apellido_mat) {
        this.apellido_mat = apellido_mat;
    }

    public String getCorreo() {
        return this.correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getTelefono() {
        return this.telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public Distrito getDistrito() {
        return this.distrito;
    }

    public void setDistrito(Distrito distrito) {
        this.distrito = distrito;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFoto() {
        return this.foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

}