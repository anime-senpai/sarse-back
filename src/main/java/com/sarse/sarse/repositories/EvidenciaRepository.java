package com.sarse.sarse.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

import com.sarse.sarse.entities.Evidencia;

@Repository
public interface EvidenciaRepository extends JpaRepository<Evidencia, Integer> {
    
    @Query(value = "SELECT * FROM evidencia a WHERE a.id_reclamo = ?1", nativeQuery = true)
    List<Evidencia> findbyReclamo(Integer id_reclamo);
    
}