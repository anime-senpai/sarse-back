package com.sarse.sarse.model;
public class UsuarioDNI {
    private String dni;
    private Integer cui;
    private String apellido_paterno;
    private String apellido_materno;
    private String nombres;


    public UsuarioDNI() {
    }

    public UsuarioDNI(String dni, Integer cui, String apellido_paterno, String apellido_materno, String nombres) {
        this.dni = dni;
        this.cui = cui;
        this.apellido_paterno = apellido_paterno;
        this.apellido_materno = apellido_materno;
        this.nombres = nombres;
    }

    public String getDni() {
        return this.dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public Integer getCui() {
        return this.cui;
    }

    public void setCui(Integer cui) {
        this.cui = cui;
    }

    public String getApellido_paterno() {
        return this.apellido_paterno;
    }

    public void setApellido_paterno(String apellido_paterno) {
        this.apellido_paterno = apellido_paterno;
    }

    public String getApellido_materno() {
        return this.apellido_materno;
    }

    public void setApellido_materno(String apellido_materno) {
        this.apellido_materno = apellido_materno;
    }

    public String getNombres() {
        return this.nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

}