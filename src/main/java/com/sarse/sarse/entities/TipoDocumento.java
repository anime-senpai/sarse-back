package com.sarse.sarse.entities;

import javax.persistence.*;


@Entity
@Table(name = "tipo_documento")
public class TipoDocumento {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_tipo_documento")
    private Integer id_tipo_documento;

    @Column(name = "nombre",length = 20)	
    private String nombre;

    @Column(name = "longitud")	
    private Integer longitud;

    public TipoDocumento() {
    }

    public TipoDocumento(Integer id_tipo_documento, String nombre, Integer longitud) {
        this.id_tipo_documento = id_tipo_documento;
        this.nombre = nombre;
        this.longitud = longitud;
    }

    public Integer getId_tipo_documento() {
        return this.id_tipo_documento;
    }

    public void setId_tipo_documento(Integer id_tipo_documento) {
        this.id_tipo_documento = id_tipo_documento;
    }

    public String getNombre() {
        return this.nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Integer getLongitud() {
        return this.longitud;
    }

    public void setLongitud(Integer longitud) {
        this.longitud = longitud;
    }
    
}