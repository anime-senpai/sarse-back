package com.sarse.sarse.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Date;

import com.sarse.sarse.entities.Reclamo;
import com.sarse.sarse.entities.Motivo;
import com.sarse.sarse.entities.Usuario;
import com.sarse.sarse.entities.Solicitud;
import com.sarse.sarse.model.SolicitudRegistrar;
import com.sarse.sarse.model.RespuestaReasignacion;
import com.sarse.sarse.repositories.SolicitudRepository;
import com.sarse.sarse.repositories.ReclamoRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SolicitudService {
    @Autowired
    private SolicitudRepository solicitudRepository;

    @Autowired
    private ReclamoRepository reclamoRepository;

    
    public List<Solicitud> getSolicitudes() {
        List<Solicitud> lista = solicitudRepository.findAll();
        for(Solicitud solicitud : lista) {
            Reclamo element = solicitud.getReclamo();
            if (element.getFecha_registro()!=null)
		        element.setFecha_registro_str();
            
            if (element.getFecha_respuesta()!=null)
                element.setFecha_respuesta_str();

            solicitud.setReclamo(element);
		}
        return lista;
    }

    public List<Solicitud> getSolicitudesxEspecialista(Integer idUsuario) {
        List<Solicitud> lista = solicitudRepository.findbyEspecialista(idUsuario);
        
        return lista;
    }

    public Integer registrarSolicitud(SolicitudRegistrar solicitud){
        Reclamo r = reclamoRepository.findById1(solicitud.getId());
        Motivo m = new Motivo();
        Usuario u = new Usuario();
        Solicitud s = new Solicitud();

        m.setId_motivo(solicitud.getMotivo());
        r.setMotivo(m);
        r = reclamoRepository.save(r);

        u.setId_usuario(solicitud.getId_usuario());

        s.setSolicitante(u);
        s.setReclamo(r);
        s.setEstado(0);
        s.setMotivo(solicitud.getTexto());
        s = solicitudRepository.save(s);
        return s.getId_solicitud();
    }

    public Integer cancelarSolicitud(Integer idSolicitud){
        Solicitud s = solicitudRepository.findbyId1(idSolicitud);
        s.setEstado(3);
        s = solicitudRepository.save(s);
        return s.getId_solicitud();
    }

    public Integer aceptarSolicitud(RespuestaReasignacion solicitud){
        Solicitud s = solicitudRepository.findbyId1(solicitud.getId());
        Usuario nuevo = new Usuario();
        Usuario admin = new Usuario();
        Reclamo r = s.getReclamo();

        nuevo.setId_usuario(solicitud.getId_nuevo());
        admin.setId_usuario(solicitud.getId_admin());

        //actualiza la solicitud
        s.setAdmin(admin);
        s.setNuevo(nuevo);
        s.setEstado(1);
        s.setComentario(solicitud.getTexto());
        s = solicitudRepository.save(s);

        //cambia el especialista
        r.setEspecialista(nuevo);
        r = reclamoRepository.save(r);

        return s.getId_solicitud();
    }

    public Integer denegarSolicitud(RespuestaReasignacion solicitud){
        Solicitud s = solicitudRepository.findbyId1(solicitud.getId());
        Usuario admin = new Usuario();
        Reclamo r = s.getReclamo();

        admin.setId_usuario(solicitud.getId_admin());

        //actualiza la solicitud
        s.setAdmin(admin);
        s.setEstado(2);
        s.setComentario(solicitud.getTexto());
        s = solicitudRepository.save(s);
        
        return s.getId_solicitud();
    }

    
}