package com.sarse.sarse.entities;

import javax.persistence.*;

@Entity
@Table(name = "distrito")
public class Distrito {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_distrito")
    private Integer id_distrito;

    @ManyToOne
    @JoinColumn(name = "id_provincia")
    private Provincia provincia;

    @Column(name = "nombre",length = 20)	
    private String nombre;
    


    public Integer getId_distrito() {
        return this.id_distrito;
    }

    public void setId_distrito(Integer id_distrito) {
        this.id_distrito = id_distrito;
    }

    public Provincia getProvincia() {
        return this.provincia;
    }

    public void setProvincia(Provincia provincia) {
        this.provincia = provincia;
    }

    public String getNombre() {
        return this.nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Distrito() {
    }

    public Distrito(Integer id_distrito, Provincia provincia, String nombre) {
        this.id_distrito = id_distrito;
        this.provincia = provincia;
        this.nombre = nombre;
    }
    
}