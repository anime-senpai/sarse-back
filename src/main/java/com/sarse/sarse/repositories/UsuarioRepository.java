package com.sarse.sarse.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.data.jpa.repository.Modifying;

import java.util.List;

import com.sarse.sarse.entities.Usuario;

@Repository
public interface UsuarioRepository extends JpaRepository<Usuario, Integer> {
    
    @Query(value = "SELECT * FROM usuario u WHERE u.correo = ?1 and u.estado = 1", nativeQuery = true)
	Usuario findByCorreo(String correo);
    
    @Query(value = "SELECT * FROM usuario u WHERE u.id_usuario = ?1", nativeQuery = true)
	Usuario buscarUsuario(Integer id_usuario);
    
    @Query(value = "SELECT * FROM usuario u WHERE u.estado = 1 order by fecha_registro DESC", nativeQuery = true)
	List<Usuario> findAllActivo();
    
    @Query(value = "SELECT * FROM usuario u WHERE u.estado = 1 and u.id_rol = ?1 order by apellido_pat DESC", nativeQuery = true)
	List<Usuario> findAllByRol(Integer id_rol);
    
    @Query(value = "SELECT * FROM usuario u WHERE u.estado = 1 and (u.id_rol = 1 or u.id_rol = 4) order by apellido_pat DESC", nativeQuery = true)
    List<Usuario> findAllEspAndSup();
    
    @Modifying
    @Transactional
    @Query(value = "INSERT INTO especialista_x_motivo (id_motivo,id_especialista,rating,activo) SELECT id_motivo, ?1 as id_especialista, 0 as rating, 1 as activo FROM motivo", nativeQuery = true)
	void registrarEspecialista(Integer id_esp);
    
    @Modifying
    @Transactional
    @Query(value = "UPDATE especialista_x_motivo SET activo=0 WHERE id_especialista = ?1", nativeQuery = true)
	void eliminarEspecialista(Integer id_esp);
}