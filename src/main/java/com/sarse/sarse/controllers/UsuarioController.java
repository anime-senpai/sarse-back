package com.sarse.sarse.controller;
import java.io.*; 

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import java.util.List;

import com.sarse.sarse.entities.Usuario;
import com.sarse.sarse.model.UsuarioLogin;
import com.sarse.sarse.model.UsuarioRegistrar;
import com.sarse.sarse.model.UsuarioDNI;
import com.sarse.sarse.model.EditarCuenta;
import com.sarse.sarse.model.CambioPass;
import com.sarse.sarse.services.UsuarioService;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

@RestController
@RequestMapping(value = "/usuario")
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.DELETE, RequestMethod.PUT})
public class UsuarioController {

    @Autowired
    private UsuarioService usuarioService;

    private static String UPLOADED_FOLDER = "C:/Users/andre/OneDrive/Desktop/PUCP/Tesis/Sarse/front/ejemplo-router/vue-material-admin/public/static/usuarios";


    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public Usuario login(@RequestBody UsuarioLogin usuario) {
        return usuarioService.login(usuario);
    }

    @RequestMapping(value = "", method = RequestMethod.GET)
    public List<Usuario> listUsuarios() {
        List<Usuario> lista = usuarioService.getUsuarios();
        return lista;
    }

    @RequestMapping(value = "/combo", method = RequestMethod.GET)
    public List<Usuario> listEspAndSup() {
        List<Usuario> lista = usuarioService.getEspAndSup();
        return lista;
    }

    @RequestMapping(value = "/rol/{id_rol}", method = RequestMethod.GET)
    public List<Usuario> listUsuariosxRol(@PathVariable("id_rol") Integer id_rol) {
        List<Usuario> lista = usuarioService.getUsuariosxRol(id_rol);
        return lista;
    }

    @RequestMapping(value = "/{id_usuario}", method = RequestMethod.GET)
    public Usuario obtenerUsuario(@PathVariable("id_usuario") Integer id_usuario) {
        Usuario u = usuarioService.getUsuario(id_usuario);
        return u;
    }

    @RequestMapping(value = "/registrar", method = RequestMethod.POST)
    public Integer registrarUsuario(@RequestBody UsuarioRegistrar usuario) {
        Integer id_usuario = usuarioService.registrarUsuario(usuario);
        return id_usuario;
    }

    @RequestMapping(value = "/registrar/ciudadano", method = RequestMethod.POST)
    public Integer registrarCiudadano(@RequestBody UsuarioRegistrar usuario) {
        Integer id_usuario = usuarioService.registrarCiudadano(usuario);
        return id_usuario;
    }

    @RequestMapping(value = "/validar", method = RequestMethod.POST)
    public Integer validarCorreo(@RequestBody UsuarioRegistrar usuario) {
        return usuarioService.validarCorreo(usuario.getCorreo());
    }

    @RequestMapping(value = "/editar", method = RequestMethod.POST)
    public Integer editarUsuario(@RequestBody UsuarioRegistrar usuario) {
        Integer id_usuario = usuarioService.editarUsuario(usuario);
        return id_usuario;
    }

    @RequestMapping(value = "/eliminar/{id_eliminar}", method = RequestMethod.POST)
    public Integer eliminarUsuario(@PathVariable(value = "id_eliminar") Integer id_eliminar) {
        Integer id_usuario = usuarioService.eliminarUsuario(id_eliminar);
        return id_usuario;
    }

    @RequestMapping(value = "/subirImg/{id_usuario}", method = RequestMethod.POST)
    public Integer subirFoto(@RequestParam("file") MultipartFile file, @PathVariable("id_usuario") Integer id_usuario) {
        File dir = new File(UPLOADED_FOLDER);
        String nombre = "usuario_"+ id_usuario.toString()+".jpg";

        try {
            byte[] bytes = file.getBytes();

            if (!dir.exists())
                dir.mkdirs();

            File uploadFile = new File(dir.getAbsolutePath()
                    + File.separator + nombre);
            BufferedOutputStream outputStream = new BufferedOutputStream(
                    new FileOutputStream(uploadFile));
            outputStream.write(bytes);
            outputStream.close();

        } catch (Exception e) {
            System.out.println("fracaso");
            System.out.println(e);
            return 0;
        }
		return id_usuario;
	}

    @RequestMapping(value = "/cambiarpass", method = RequestMethod.POST)
    public Integer cambiarPass(@RequestBody CambioPass usuario) {
        return usuarioService.cambiarPass(usuario);
    }

    @RequestMapping(value = "/olvidarpass", method = RequestMethod.POST)
    public Integer olvidarPass(@RequestBody UsuarioLogin usuario) {
        return usuarioService.olvidarPass(usuario.getCorreo());
    }

    @RequestMapping(value = "/editar/cuenta", method = RequestMethod.POST)
    public Integer editarCuenta(@RequestBody EditarCuenta usuario) {
        return usuarioService.editarCuenta(usuario);
    }

    @RequestMapping(value = "/validar/{dni}/{cui}", method = RequestMethod.GET)
    public UsuarioDNI validarDNI(@PathVariable("dni") String dni, @PathVariable("cui") Integer cui) {
        UsuarioDNI u = usuarioService.validarDNI(dni,cui);
        return u;
    }

    
}