package com.sarse.sarse.entities;

import javax.persistence.*;


@Entity
@Table(name = "suministrador")
public class Suministrador {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_suministrador")
    private Integer id_suministrador;

    @Column(name = "nombre",length = 100)	
    private String nombre;


    public Suministrador() {
    }


    public Suministrador(Integer id_suministrador, String nombre) {
        this.id_suministrador = id_suministrador;
        this.nombre = nombre;
    }

    public Integer getId_suministrador() {
        return this.id_suministrador;
    }

    public void setId_suministrador(Integer id_suministrador) {
        this.id_suministrador = id_suministrador;
    }

    public String getNombre() {
        return this.nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
}