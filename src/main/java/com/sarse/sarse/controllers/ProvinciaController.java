package com.sarse.sarse.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;

import com.sarse.sarse.entities.Provincia;
import com.sarse.sarse.services.ProvinciaService;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

@RestController
@RequestMapping(value = "/provincia")
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.DELETE, RequestMethod.PUT})
public class ProvinciaController {

    @Autowired
    private ProvinciaService provinciaService;

    @RequestMapping(value = "", method = RequestMethod.GET)
    public List<Provincia> listProvincias() {
        List<Provincia> lista = provinciaService.getProvincias();
        return lista;
    }

    @RequestMapping(value = "/departamento/{id_departamento}", method = RequestMethod.GET)
    public List<Provincia> listProvinciasxDepartamento(@PathVariable(value = "id_departamento") Integer id_departamento) {
        List<Provincia> lista = provinciaService.getProvinciasxDepartamento(id_departamento);
        return lista;
    }
    
}