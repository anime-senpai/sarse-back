package com.sarse.sarse.services;


import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.crypto.Data;

import org.apache.tomcat.util.codec.binary.Base64;

public class EncryptService {
	
	private static Base64 base64 = new Base64(true);
	
	public static String encrypt(String strClearText,String strKey){
		
		try {
			SecretKeySpec skeyspec=new SecretKeySpec(strKey.getBytes("UTF8"),"Blowfish");
			Cipher cipher=Cipher.getInstance("Blowfish");
			cipher.init(Cipher.ENCRYPT_MODE, skeyspec);
			
			return base64.encodeToString(cipher.doFinal(strClearText.getBytes("UTF8")));			
			
		} catch (Exception e) {	
			e.printStackTrace();
			return null;
		}
	}
	
	public static String decrypt(String strEncrypted,String strKey){		
		try {
	        byte[] encryptedData = base64.decodeBase64(strEncrypted);
	        SecretKeySpec key = new SecretKeySpec(strKey.getBytes("UTF8"), "Blowfish");
	        Cipher cipher = Cipher.getInstance("Blowfish");
	        cipher.init(Cipher.DECRYPT_MODE, key);
	        byte[] decrypted = cipher.doFinal(encryptedData);
	        return new String(decrypted); 
			
			
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
}
