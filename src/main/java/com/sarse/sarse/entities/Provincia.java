package com.sarse.sarse.entities;

import javax.persistence.*;


@Entity
@Table(name = "provincia")
public class Provincia {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_provincia")
    private Integer id_provincia;

    @ManyToOne
    @JoinColumn(name = "id_departamento")
    private Departamento departamento;

    @Column(name = "nombre",length = 70)	
    private String nombre;
    


    public Integer getId_provincia() {
        return this.id_provincia;
    }

    public void setId_provincia(Integer id_provincia) {
        this.id_provincia = id_provincia;
    }

    public Departamento getDepartamento() {
        return this.departamento;
    }

    public void setDepartamento(Departamento departamento) {
        this.departamento = departamento;
    }

    public String getNombre() {
        return this.nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    

    public Provincia() {
    }

    public Provincia(Integer id_provincia, Departamento departamento, String nombre) {
        this.id_provincia = id_provincia;
        this.departamento = departamento;
        this.nombre = nombre;
    }

}