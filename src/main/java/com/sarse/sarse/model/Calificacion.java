package com.sarse.sarse.model;
public class Calificacion {
    private Integer id;
    private Double calificacion1;
    private Double calificacion2;
    private Double calificacion3;


    public Calificacion() {
    }

    public Calificacion(Integer id, Double calificacion1, Double calificacion2, Double calificacion3) {
        this.id = id;
        this.calificacion1 = calificacion1;
        this.calificacion2 = calificacion2;
        this.calificacion3 = calificacion3;
    }

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Double getCalificacion1() {
        return this.calificacion1;
    }

    public void setCalificacion1(Double calificacion1) {
        this.calificacion1 = calificacion1;
    }

    public Double getCalificacion2() {
        return this.calificacion2;
    }

    public void setCalificacion2(Double calificacion2) {
        this.calificacion2 = calificacion2;
    }

    public Double getCalificacion3() {
        return this.calificacion3;
    }

    public void setCalificacion3(Double calificacion3) {
        this.calificacion3 = calificacion3;
    }

}