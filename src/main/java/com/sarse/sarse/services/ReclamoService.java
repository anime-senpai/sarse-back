package com.sarse.sarse.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Date;

import com.sarse.sarse.model.ReclamoRegistrar;
import com.sarse.sarse.model.Comentario;
import com.sarse.sarse.model.Calificacion;
import com.sarse.sarse.entities.Reclamo;
import com.sarse.sarse.entities.Rol;
import com.sarse.sarse.entities.Motivo;
import com.sarse.sarse.entities.TipoDocumento;
import com.sarse.sarse.entities.Usuario;
import com.sarse.sarse.entities.Suministrador;
import com.sarse.sarse.entities.Distrito;
import com.sarse.sarse.repositories.ReclamoRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Scanner;
import org.json.JSONObject;
import java.io.*;

@Service
public class ReclamoService {

    @Autowired
    private ReclamoRepository reclamoRepository;

    @Autowired
    private UsuarioService usuarioService;

    public List<Reclamo> getReclamos() {
        List<Reclamo> lista = reclamoRepository.findAll();
        
		for(Reclamo element : lista) {
            if (element.getFecha_registro()!=null)
		        element.setFecha_registro_str();
            
            if (element.getFecha_respuesta()!=null)
                element.setFecha_respuesta_str();
		}
        return lista;
    }
    //CALIFICACIONES
    public List<Reclamo> getReclamosCalificadosxEspecialista(Integer idUsuario) {
        List<Reclamo> lista = reclamoRepository.findCalificadosbyEspecialista(idUsuario);
        
		for(Reclamo element : lista) {
            if (element.getFecha_registro()!=null)
		        element.setFecha_registro_str();
            
            if (element.getFecha_respuesta()!=null)
                element.setFecha_respuesta_str();
		}
        return lista;
    }

    public List<Reclamo> getReclamosCalificados() {
        List<Reclamo> lista = reclamoRepository.findCalificados();
        
		for(Reclamo element : lista) {
            if (element.getFecha_registro()!=null)
		        element.setFecha_registro_str();
            
            if (element.getFecha_respuesta()!=null)
                element.setFecha_respuesta_str();
		}
        return lista;
    }

    public Integer calificarReclamo(Calificacion cal){
        Reclamo r = reclamoRepository.findById1(cal.getId());

        r.setCalificacion_1(cal.getCalificacion1());
        r.setCalificacion_2(cal.getCalificacion2());
        r.setCalificacion_3(cal.getCalificacion3());
        r = reclamoRepository.save(r);
        return r.getId_reclamo();
    }


    //FIN CALIFICACIONES
    public List<Reclamo> getReclamosxCiudadano(Integer idUsuario) {
        List<Reclamo> lista = reclamoRepository.findbyCiudadano(idUsuario);
        
		for(Reclamo element : lista) {
            if (element.getFecha_registro()!=null)
		        element.setFecha_registro_str();
            
            if (element.getFecha_respuesta()!=null)
                element.setFecha_respuesta_str();
		}
        return lista;
    }

    public List<Reclamo> getReclamosxEspecialista(Integer idUsuario) {
        List<Reclamo> lista = reclamoRepository.findbyEspecialista(idUsuario);
        
		for(Reclamo element : lista) {
            if (element.getFecha_registro()!=null)
		        element.setFecha_registro_str();
            
            if (element.getFecha_respuesta()!=null)
                element.setFecha_respuesta_str();
		}
        return lista;
    }



    private static String streamToString(InputStream inputStream) {
        String text = new Scanner(inputStream, "UTF-8").useDelimiter("\\Z").next();
        return text;
    }



    public Integer registrarReclamo(ReclamoRegistrar reclamo) {
        Reclamo r = new Reclamo();
        Motivo m = new Motivo();
        Usuario especialista = new Usuario();
        Usuario c = new Usuario();
        Date fecha = new Date();
        Suministrador s = new Suministrador();
        Distrito d = new Distrito();
        Integer id_motivo = 1;

        String textoJson = "{\"texto\": \"" + reclamo.getReclamoRegistrar() + "\"}";

        String jsonString = null;
        String link = "http://54.165.247.239:5000/clasificar";
        
        
        try {
            URL url = new URL(link);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setInstanceFollowRedirects(false);
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setRequestProperty("Accept", "application/json");
            connection.setRequestProperty("User-Agent", "PostmanRuntime/7.26.1");
            connection.setRequestProperty("charset", "UTF-8");


            OutputStream os = connection.getOutputStream();
            byte[] input = textoJson.getBytes("UTF-8");
            os.write(input, 0, input.length);

            //System.out.println(os.toString());
            
            connection.connect();
            
            InputStream inStream = connection.getInputStream();
            jsonString = streamToString(inStream); // input stream to string
            System.out.println(jsonString);

        } catch (IOException ex) {
            ex.printStackTrace();
        }

        JSONObject jsonObj = new JSONObject(jsonString);
        id_motivo = jsonObj.getInt("motivo");

        Integer id_esp = reclamoRepository.seleccionarEspecialista(id_motivo);

        c.setId_usuario(reclamo.getUsuarioRegistrar());
        m.setId_motivo(id_motivo);
        especialista.setId_usuario(id_esp);
        s.setId_suministrador(reclamo.getSuministradorRegistrar());
        d.setId_distrito(reclamo.getDistritoRegistrar());
        

        //crear ciudadano

        r.setMotivo(m);
        r.setDistrito(d);
        r.setEspecialista(especialista);
        r.setCiudadano(c);
        r.setFecha_registro(fecha);
        r.setTexto(reclamo.getReclamoRegistrar());
        r.setSuministrador(s);
        r.setNum_suministro(reclamo.getNumSuministroRegistrar());
        r.setDomicilio(reclamo.getDomicilioRegistrar());
        r.setEstado(0);

        r = reclamoRepository.save(r);
        return r.getId_reclamo();
    }

    public Integer comentarReclamo(Comentario comentario){
        Reclamo r = reclamoRepository.findById1(comentario.getId());
        r.setComentario(comentario.getTexto());
        r = reclamoRepository.save(r);
        return r.getId_reclamo();
    }

    public Integer responderReclamo(Comentario comentario, Integer id_esp){
        Reclamo r = reclamoRepository.findById1(comentario.getId());
        Motivo m = new Motivo();
        Date d = new Date();
        m.setId_motivo(comentario.getMotivo());
        
        Usuario especialista = new Usuario();
        especialista.setId_usuario(id_esp);
        r.setEspecialista(especialista);

        r.setMotivo(m);
        r.setRespuesta(comentario.getTexto());
        r.setEstado(1);
        r.setFecha_respuesta(d);
        r = reclamoRepository.save(r);
        return r.getId_reclamo();
    }

    public Integer cancelarReclamo(Integer id_reclamo){
        Reclamo r = reclamoRepository.findById1(id_reclamo);
        r.setEstado(2);
        r = reclamoRepository.save(r);
        return r.getId_reclamo();
    }
    
}