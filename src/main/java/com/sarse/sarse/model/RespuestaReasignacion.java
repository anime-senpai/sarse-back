package com.sarse.sarse.model;
public class RespuestaReasignacion {
    private Integer id;
    private Integer id_admin;
    private Integer id_nuevo;
    private String texto;


    public RespuestaReasignacion() {
    }

    public RespuestaReasignacion(Integer id, Integer id_admin, Integer id_nuevo, String texto) {
        this.id = id;
        this.id_admin = id_admin;
        this.id_nuevo = id_nuevo;
        this.texto = texto;
    }

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getId_admin() {
        return this.id_admin;
    }

    public void setId_admin(Integer id_admin) {
        this.id_admin = id_admin;
    }

    public Integer getId_nuevo() {
        return this.id_nuevo;
    }

    public void setId_nuevo(Integer id_nuevo) {
        this.id_nuevo = id_nuevo;
    }

    public String getTexto() {
        return this.texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }

}