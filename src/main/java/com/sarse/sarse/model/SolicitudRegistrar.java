package com.sarse.sarse.model;
public class SolicitudRegistrar {
    private Integer id;
    private String texto;
    private Integer id_usuario;
    private Integer motivo;


    public SolicitudRegistrar() {
    }


    public SolicitudRegistrar(Integer id, String texto, Integer id_usuario, Integer motivo) {
        this.id = id;
        this.texto = texto;
        this.id_usuario = id_usuario;
        this.motivo = motivo;
    }


    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTexto() {
        return this.texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }

    public Integer getId_usuario() {
        return this.id_usuario;
    }

    public void setId_usuario(Integer id_usuario) {
        this.id_usuario = id_usuario;
    }

    public Integer getMotivo() {
        return this.motivo;
    }

    public void setMotivo(Integer motivo) {
        this.motivo = motivo;
    }

}