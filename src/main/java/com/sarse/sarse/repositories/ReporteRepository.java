package com.sarse.sarse.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

import com.sarse.sarse.model.Reporte;

@Repository
public interface ReporteRepository extends JpaRepository<Reporte, Integer>  {

    @Query(value = "SELECT m.id_motivo as id, Count(*) as cant, m.desc as label, null as porc FROM reclamo r " +
                   "LEFT JOIN motivo m ON r.id_motivo=m.id_motivo GROUP BY m.id_motivo, m.desc ORDER BY m.id_motivo ASC", nativeQuery = true)
    List<Reporte> reporteMotivo();

    @Query(value = "SELECT DATE_FORMAT(fecha_registro, '%d')*100 + DATE_FORMAT(fecha_registro, '%m')*10 + DATE_FORMAT(fecha_registro, '%y') as id, DATE_FORMAT(fecha_registro, '%d-%m-%Y') as label, count(*) as cant, null as porc " +
                   "FROM sarse.reclamo GROUP BY  DATE_FORMAT(fecha_registro, '%d-%m-%Y') ORDER BY DATE_FORMAT(fecha_registro, '%d-%m-%Y') ASC", nativeQuery = true)
    List<Reporte> reporteDia();

    @Query(value = "SELECT DATE_FORMAT(fecha_respuesta, '%d-%m-%Y')-DATE_FORMAT(fecha_registro, '%d-%m-%Y') as id, DATE_FORMAT(fecha_respuesta, '%d-%m-%Y')-DATE_FORMAT(fecha_registro, '%d-%m-%Y')+1 as label, count(*) as cant, null as porc " +
                   "FROM reclamo r WHERE r.estado=1 GROUP BY  DATE_FORMAT(fecha_respuesta, '%d-%m-%Y')-DATE_FORMAT(fecha_registro, '%d-%m-%Y') ORDER BY DATE_FORMAT(fecha_respuesta, '%d-%m-%Y')-DATE_FORMAT(fecha_registro, '%d-%m-%Y') ASC", nativeQuery = true)
    List<Reporte> reporteTiempo();
}