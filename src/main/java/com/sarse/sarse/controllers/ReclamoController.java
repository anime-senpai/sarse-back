package com.sarse.sarse.controller;
import java.io.*; 

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.sarse.sarse.model.Comentario;
import java.util.List;

import com.sarse.sarse.entities.Reclamo;
import com.sarse.sarse.model.ReclamoRegistrar;
import com.sarse.sarse.model.Calificacion;
import com.sarse.sarse.services.ReclamoService;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

@RestController
@RequestMapping(value = "/reclamo")
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.DELETE, RequestMethod.PUT})
public class ReclamoController {

    @Autowired
    private ReclamoService reclamoService;

    @RequestMapping(value = "", method = RequestMethod.GET)
    public List<Reclamo> listReclamos() {
        List<Reclamo> lista = reclamoService.getReclamos();
        return lista;
    }

    @RequestMapping(value = "/{idUsuario}", method = RequestMethod.GET)
    public List<Reclamo> listReclamosbyCiudadano(@PathVariable(value = "idUsuario") Integer idUsuario) {
        List<Reclamo> lista = reclamoService.getReclamosxCiudadano(idUsuario);
        return lista;
    }

    @RequestMapping(value = "/especialista/{idUsuario}", method = RequestMethod.GET)
    public List<Reclamo> listReclamosbyEspecialista(@PathVariable(value = "idUsuario") Integer idUsuario) {
        List<Reclamo> lista = reclamoService.getReclamosxEspecialista(idUsuario);
        return lista;
    }

    @RequestMapping(value = "/registrar", method = RequestMethod.POST)
    public Integer registrarReclamo(@RequestBody ReclamoRegistrar reclamo) {
        Integer id_reclamo = reclamoService.registrarReclamo(reclamo);
        return id_reclamo;
    }

    @RequestMapping(value = "/cancelar/{id_reclamo}", method = RequestMethod.POST)
    public Integer cancelarReclamo(@PathVariable(value = "id_reclamo") Integer id_reclamo) {
        return reclamoService.cancelarReclamo(id_reclamo);
    }

    @RequestMapping(value = "/comentar", method = RequestMethod.POST)
    public Integer comentarReclamo(@RequestBody Comentario reclamo) {
        Integer id_reclamo = reclamoService.comentarReclamo(reclamo);
        return id_reclamo;
    }

    @RequestMapping(value = "/responder/{id_usuario}", method = RequestMethod.POST)
    public Integer responderReclamo(@RequestBody Comentario reclamo, @PathVariable("id_usuario") Integer id_usuario) {
        Integer id_reclamo = reclamoService.responderReclamo(reclamo,id_usuario);
        return id_reclamo;
    }

    @RequestMapping(value = "/calificar", method = RequestMethod.POST)
    public Integer calificarReclamo(@RequestBody Calificacion cal) {
        Integer id_reclamo = reclamoService.calificarReclamo(cal);
        return id_reclamo;
    }

    @RequestMapping(value = "/calificados", method = RequestMethod.GET)
    public List<Reclamo> getReclamosCalificados() {
        List<Reclamo> lista = reclamoService.getReclamosCalificados();
        return lista;
    }

    @RequestMapping(value = "/calificados/{idUsuario}", method = RequestMethod.GET)
    public List<Reclamo> getReclamosCalificadosxEspecialista(@PathVariable(value = "idUsuario") Integer idUsuario) {
        List<Reclamo> lista = reclamoService.getReclamosCalificadosxEspecialista(idUsuario);
        return lista;
    }
}