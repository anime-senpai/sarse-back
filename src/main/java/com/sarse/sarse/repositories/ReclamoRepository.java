package com.sarse.sarse.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

import com.sarse.sarse.entities.Reclamo;

@Repository
public interface ReclamoRepository extends JpaRepository<Reclamo, Integer> {
    
    @Query(value = "SELECT * FROM reclamo a WHERE a.id_ciudadano = ?1", nativeQuery = true)
    List<Reclamo> findbyCiudadano(Integer idUsuario);

    @Query(value = "SELECT * FROM reclamo a WHERE a.id_reclamo = ?1", nativeQuery = true)
    Reclamo findById1(Integer idReclamo);
    
    @Query(value = "SELECT * FROM reclamo a WHERE a.id_especialista = ?1", nativeQuery = true)
    List<Reclamo> findbyEspecialista(Integer idUsuario);
    
    @Query(value = "SELECT * FROM reclamo a WHERE a.calificacion_1 != 1", nativeQuery = true)
    List<Reclamo> findCalificados();
    
    @Query(value = "SELECT * FROM reclamo a WHERE a.id_especialista = ?1 and a.calificacion_1 != 1", nativeQuery = true)
    List<Reclamo> findCalificadosbyEspecialista(Integer idUsuario);
    
    @Query(value = "SELECT id_especialista FROM especialista_x_motivo where id_motivo = ?1 and activo = 1 and id_especialista in (SELECT id_usuario FROM (SELECT u.id_usuario, ifnull(b.cant,0) as cant FROM usuario u left join (SELECT id_especialista, count(*) as cant FROM reclamo where estado=0 group by id_especialista) b on u.id_usuario = b.id_especialista where id_rol = 1 having cant = (SELECT min(cant) FROM (SELECT u.id_usuario, ifnull(a.cant,0) cant FROM usuario u left join (SELECT id_especialista, count(*) as cant FROM reclamo where estado=0 group by id_especialista) a on u.id_usuario = a.id_especialista where u.id_rol = 1) a))f) ORDER BY rating DESC LIMIT 1;", nativeQuery = true)
    Integer seleccionarEspecialista(Integer id_motivo);
}