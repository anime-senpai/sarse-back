package com.sarse.sarse.services;

import java.util.ArrayList;
import java.util.List;

import com.sarse.sarse.entities.Motivo;
import com.sarse.sarse.repositories.MotivoRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MotivoService {

    @Autowired
    private MotivoRepository motivoRepository;

    public List<Motivo> getMotivos() {
        List<Motivo> lista = motivoRepository.findAll();
        return lista;
    }
    
}